import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo.mile.app/landing')

WebUI.click(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/p_Request Demo'))

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Full name_name'), 'Testing Katalon')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Email_email'), 'tes123@yahoo.com')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Phone number_phone'), '081422163922')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Company name_organization_name'), 'Testing123')

WebUI.click(findTestObject('Page_Mileapp  Request Demo/button_Request Demo'))

WebUI.click(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/p_Request Demo'))

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Full name_name'), '')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Email_email'), 'tes12@gmail.com')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Phone number_phone'), '081422163922')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Company name_organization_name'), 'Tes123')

WebUI.click(findTestObject('Page_Mileapp  Request Demo/button_Request Demo'))

WebUI.refresh()

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Full name_name'), 'tes123')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Email_email'), '')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Phone number_phone'), '081422163922')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Company name_organization_name'), 'Tes123')

WebUI.click(findTestObject('Page_Mileapp  Request Demo/button_Request Demo'))

WebUI.refresh()

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Full name_name'), 'tes123')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Email_email'), 'tes123@gmail.com')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Phone number_phone'), '')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Company name_organization_name'), 'Tes123')

WebUI.click(findTestObject('Page_Mileapp  Request Demo/button_Request Demo'))

WebUI.refresh()

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Full name_name'), 'tes123')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Email_email'), 'tes123@gmail.com')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Phone number_phone'), '08661231312')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Company name_organization_name'), '')

WebUI.click(findTestObject('Page_Mileapp  Request Demo/button_Request Demo'))

WebUI.refresh()

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Full name_name'), 'tes123')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Email_email'), 'salahemail,com')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Phone number_phone'), '08661231312')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Company name_organization_name'), 'PT.Sayap ayam')

WebUI.click(findTestObject('Page_Mileapp  Request Demo/button_Request Demo'))

WebUI.refresh()

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Full name_name'), 'tes123')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Email_email'), 'tes@gmail.com')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Phone number_phone'), '2')

WebUI.setText(findTestObject('Page_Mileapp  Request Demo/input_Company name_organization_name'), 'PT.Sayap ayam')

WebUI.click(findTestObject('Page_Mileapp  Request Demo/button_Request Demo'))

WebUI.refresh()

WebUI.click(findTestObject('Page_Mileapp  Request Demo/a_Back to Mile'))

WebUI.click(findTestObject('Page_Mileapp - The best Logistics Platform to run your business/a_Login'))

WebUI.setText(findTestObject('Page_Mileapp  Login/input_Back to Mile_organization'), '@#$%$$')

WebUI.refresh()

WebUI.setText(findTestObject('Page_Mileapp  Login/input_Back to Mile_organization'), 'tes123@gmail.com')

WebUI.click(findTestObject('Page_Mileapp  Login/a_Back to Mile'))

WebUI.refresh()

WebUI.closeBrowser()

